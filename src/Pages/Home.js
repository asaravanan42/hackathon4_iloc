import React from 'react'
import { Button } from 'antd';
import { GoogleMap, LoadScript, Polygon } from '@react-google-maps/api';
import { AppContext } from '../App';
const center = { lat: 13.040995722619396, lng: 80.23216920046744,}
const mapContainerStyle = {
    height: "100%",
    width: "100%"
}


export default function Home() {
    const App = React.useContext(AppContext);


    return (
        <div className="container-fluid p-5" style={{
            height: "100vh"
        }}>
            <div className="row h-100">
                <div className="col-6 border-right">
                    <LoadScript
                        googleMapsApiKey="AIzaSyBHM1cKTeGW1bMH9CG4ZtNasJnFP_ig7uk"
                    >
                        <GoogleMap
                            mapContainerStyle={mapContainerStyle}
                            center={center}
                            zoom={15}
                        >
                            { /* Child components, such as markers, info windows, etc. */}
                            <></>
                            {/* <Polygon
                      onLoad={onLoad}
                      paths={paths}
                      options={options}
                    /> */}
                        </GoogleMap>
                    </LoadScript>
                </div>
                <div className="col-6 h-100">
                    <div className="d-flex justify-content-center align-items-center h-100">
                        <div>
                            <img src={"https://cdn0.iconfinder.com/data/icons/maps-and-navigation-3-1/52/113-512.png"} width="100" />
                            <p className="display-3">Know where to start your business</p>
                            <Button size={"large"} type="primary" onClick={()=>{
                                App?.methods?.setState({
                                   type: "SET_STEP" ,
                                   payload:1
                                })
                            }}>Get Started</Button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    )
}
