import React from 'react'

const businesses = [
    {
        name: "Restaurants",
        img: "https://cdn1.iconfinder.com/data/icons/restaurant-37/128/Restaurant_2-03-512.png"
    },
    {
        name: "Saloon",
        img: "https://static.thenounproject.com/png/120169-200.png"
    },
    {
        name: "Coffee Shops",
        img: "https://cdn1.iconfinder.com/data/icons/coffee-shop-25/512/Coffee-24-512.png"
    },
    {
        name: "Hardware Store",
        img: "https://pineresearch.com/shop/wp-content/uploads/sites/2/2018/12/tools-icon.png"
    },
    {
        name: "Video Store",
        img: "https://cdn4.iconfinder.com/data/icons/relief-video-and-tv-player/32/Artboard_8-512.png"
    }
];

const BusinessEle = ({ name, img }) => {
    return <div className="mr-3 p-1 cityele">
        <div className={`mb-1`}>
            <img src={img} width="60" height="60" />
        </div>
        <div className="font-weight-bold">{name}</div>
    </div>
}

export default function ChooseBusiness() {
    return (
        <div className="container p-5">
            <p className="mt-2 lead text-muted" style={{
                fontSize: "2rem"
            }}> Choose the type of business you wanna start</p>

            <div class="search-bar-wrapper mb-5">
                <label for=""></label>
                <input type="text"
                    class="form-control" name="" id="" aria-describedby="helpId" placeholder="Search for the type of business you want to start" />
            </div>

            <div className="d-flex justify-content-center">
                {
                    businesses?.map((business) => {
                        return <BusinessEle {...business} />
                    })
                }
            </div>
        </div>
    )
}
