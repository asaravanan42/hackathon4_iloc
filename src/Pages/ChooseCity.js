import React from 'react'
import { AppContext } from '../App';

const cities = [
    {
        name: "Chennai",
        img: ""
    },
    {
        name: "Bangalore",
        img: ""
    },
    {
        name: "Hyderabad",
        img: ""
    },
    {
        name: "Mumbai",
        img: ""
    },
    {
        name: "Kolkata",
        img: ""
    }
];

const CityEle = ({ name, onCitySelect }) => {
    return <div className="mr-3 p-2 cityele" onClick={onCitySelect}>
        <div className={`city-sprite mb-1 sprite--${name?.toLowerCase()}`}></div>
        <div className="font-weight-bold">{name}</div>
    </div>
}

export default function ChooseCity() {

    const App = React.useContext(AppContext);

    const onCitySelect = () =>{
        App?.methods?.setState({
            type: "SET_STEP" ,
            payload:2
         })
    }

    return (
        <div className="container p-5">
            <p className="mt-2 lead text-muted" style={{
                fontSize: "2rem"
            }}> Select City</p>

            <div class="search-bar-wrapper mb-5">
                <label for=""></label>
                <input type="text"
                    class="form-control" name="" id="" aria-describedby="helpId" placeholder="Search for your city" />
            </div>

            <div className="d-flex justify-content-center">
                {
                    cities?.map((city) => {
                        return <CityEle {...city} onCitySelect={onCitySelect} />
                    })
                }
            </div>
        </div>
    )
}
