const AppReducer = (state, action) => {
    switch (action.type) {
        case "SET_STEP":
            return { ...state, step: action.payload };
        default:
            throw new Error();
    }
}

export default AppReducer;