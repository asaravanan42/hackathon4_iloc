import React from "react";
import './App.css';
import Home from "./Pages/Home";
import AppReducer from "./AppReducer";
import ChooseCity from "./Pages/ChooseCity";
import ChooseBusiness from "./Pages/ChooseBusiness";
export const AppContext = React.createContext(null);


function App() {

  const [state, dispatch] = React.useReducer(AppReducer, {
    step: 0
  });

  return (
    <AppContext.Provider value={{
      data: state,
      methods: {
        setState: dispatch
      }
    }}>

      <div className="App">
        {
          state?.step !== 0 && <div className="d-flex justify-content-between align-items-center">
          <div className="p-2 pl-3 pointer" onClick={()=>{
             dispatch({
              type: "SET_STEP" ,
              payload:0
           })
          }}>
            <img src={"https://cdn0.iconfinder.com/data/icons/maps-and-navigation-3-1/52/113-512.png"} width="40" />
          </div>
        </div>
        }
        
        {
          state?.step === 0 && <Home />
        }
        {
          state?.step === 1 && <ChooseCity />
        }
        {
          state?.step === 2 && <ChooseBusiness />
        }
      </div>

    </AppContext.Provider>
  );
}

export default App;
